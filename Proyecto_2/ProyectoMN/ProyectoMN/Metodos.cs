﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using info.lundin.math;

namespace ProyectoMN
{
    public partial class Metodos : Form
    {

        
        public DataSet data2;
        public DataSet data;
        public DataSet data3;
        public DataSet data4;
        public DataSet data5;
        public DataSet data6;

        public Metodos()
        {
            InitializeComponent();
        }

        private void Metodos_Load(object sender, EventArgs e)
        {
            dgvbiseccion.DataSource = data.Tables[0];
            dataGridView1.DataSource= data2.Tables[0];
            dataGridView2.DataSource = data2.Tables[0];
            dataGridView4.DataSource = data3.Tables[0];
            dgvrapshon.DataSource= data4.Tables[0];
            dgvreglafalsa.DataSource = data5.Tables[0];

            


        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        public void grafico()
        {

            double inicio;
            double final;
            double incremento;
            double x;
            double y;
            double result;
            string ecuacion2;
            string ecuacion;


            ecuacion = txtec1.Text;
            ecuacion2 = txtec2.Text;
            inicio = Convert.ToDouble(txtinicio.Text);
            final = Convert.ToDouble(txtfinal.Text);
            incremento = Convert.ToDouble(txtincremento.Text);

        
         

            DoubleValue xval = new DoubleValue();
            ExpressionParser parser = new ExpressionParser();
            parser.Values.Add("x", xval);

            x = inicio;


            

            do
            {
               

                //Evaluacion variable

                xval.Value = x; // Update value of "x"
                result = parser.Parse(ecuacion);
                y = result;



                // Agregar punto

                TablaGrafico.Series[0].Points.AddXY(x, y);
                
                // Aumento de X

                x = x + incremento;

            } while (x <= final);



            x = inicio;

            do
            {


                //Evaluacion variable

                xval.Value = x; // Update value of "x"
                result = parser.Parse(ecuacion2);
                y = result;



                // Agregar punto

                TablaGrafico.Series[1].Points.AddXY(x, y);

                // Aumento de X

                x = x + incremento;

            } while (x <= final);
            // TablaGrafico.Series[0].ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;

        }

        private void button1_Click(object sender, EventArgs e)
        {
             
            grafico();
       
        }

        private void TablaGrafico_Click(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            TablaGrafico.Series.Clear();
        }

        private void tabPage1_Click(object sender, EventArgs e)
        {

        }
    }
}
