﻿
namespace ProyectoMN
{
    partial class txtxo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(txtxo));
            this.btnn1 = new System.Windows.Forms.Button();
            this.btnn2 = new System.Windows.Forms.Button();
            this.btnn3 = new System.Windows.Forms.Button();
            this.btnn6 = new System.Windows.Forms.Button();
            this.btnn5 = new System.Windows.Forms.Button();
            this.btnn4 = new System.Windows.Forms.Button();
            this.btnn9 = new System.Windows.Forms.Button();
            this.btnn8 = new System.Windows.Forms.Button();
            this.btnn7 = new System.Windows.Forms.Button();
            this.btnn0 = new System.Windows.Forms.Button();
            this.btnx = new System.Windows.Forms.Button();
            this.BtnLimpiar = new System.Windows.Forms.Button();
            this.btnmultiplicar = new System.Windows.Forms.Button();
            this.btndivision = new System.Windows.Forms.Button();
            this.btnmenos = new System.Windows.Forms.Button();
            this.btnsuma = new System.Windows.Forms.Button();
            this.btnparentesis2 = new System.Windows.Forms.Button();
            this.btnparentensis1 = new System.Windows.Forms.Button();
            this.btnelevar = new System.Windows.Forms.Button();
            this.btne = new System.Windows.Forms.Button();
            this.dataSet1 = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataSet2 = new System.Data.DataSet();
            this.dataTable2 = new System.Data.DataTable();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dataColumn14 = new System.Data.DataColumn();
            this.txterror = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtintervalo2 = new System.Windows.Forms.TextBox();
            this.txtintervalo1 = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtx0 = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.dataSet3 = new System.Data.DataSet();
            this.dataTable3 = new System.Data.DataTable();
            this.dataColumn15 = new System.Data.DataColumn();
            this.dataColumn16 = new System.Data.DataColumn();
            this.dataColumn17 = new System.Data.DataColumn();
            this.dataColumn18 = new System.Data.DataColumn();
            this.dataColumn19 = new System.Data.DataColumn();
            this.dataSet4 = new System.Data.DataSet();
            this.dataTable4 = new System.Data.DataTable();
            this.dataColumn20 = new System.Data.DataColumn();
            this.dataColumn21 = new System.Data.DataColumn();
            this.dataColumn22 = new System.Data.DataColumn();
            this.dataColumn23 = new System.Data.DataColumn();
            this.dataColumn24 = new System.Data.DataColumn();
            this.dataColumn25 = new System.Data.DataColumn();
            this.dataSet5 = new System.Data.DataSet();
            this.dataTable5 = new System.Data.DataTable();
            this.dataColumn26 = new System.Data.DataColumn();
            this.dataColumn27 = new System.Data.DataColumn();
            this.dataColumn28 = new System.Data.DataColumn();
            this.dataColumn29 = new System.Data.DataColumn();
            this.dataColumn30 = new System.Data.DataColumn();
            this.dataColumn31 = new System.Data.DataColumn();
            this.dataColumn32 = new System.Data.DataColumn();
            this.dataColumn33 = new System.Data.DataColumn();
            this.dataColumn34 = new System.Data.DataColumn();
            this.dataColumn35 = new System.Data.DataColumn();
            this.contextMenuStrip1 = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.TxtPantallaCalculo = new System.Windows.Forms.TextBox();
            this.btncalcular = new System.Windows.Forms.Button();
            this.btnln = new System.Windows.Forms.Button();
            this.btnlog = new System.Windows.Forms.Button();
            this.btncotangente = new System.Windows.Forms.Button();
            this.Btnscosecante = new System.Windows.Forms.Button();
            this.Btnsecante = new System.Windows.Forms.Button();
            this.btntangente = new System.Windows.Forms.Button();
            this.Btnscoseno = new System.Windows.Forms.Button();
            this.Btnseno = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable4)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet5)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable5)).BeginInit();
            this.SuspendLayout();
            // 
            // btnn1
            // 
            this.btnn1.BackColor = System.Drawing.Color.Transparent;
            this.btnn1.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnn1.Location = new System.Drawing.Point(45, 34);
            this.btnn1.Margin = new System.Windows.Forms.Padding(4);
            this.btnn1.Name = "btnn1";
            this.btnn1.Size = new System.Drawing.Size(73, 53);
            this.btnn1.TabIndex = 1;
            this.btnn1.Text = "1";
            this.btnn1.UseVisualStyleBackColor = false;
            this.btnn1.Click += new System.EventHandler(this.btnn1_Click);
            // 
            // btnn2
            // 
            this.btnn2.BackColor = System.Drawing.Color.Transparent;
            this.btnn2.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnn2.Location = new System.Drawing.Point(126, 34);
            this.btnn2.Margin = new System.Windows.Forms.Padding(4);
            this.btnn2.Name = "btnn2";
            this.btnn2.Size = new System.Drawing.Size(73, 53);
            this.btnn2.TabIndex = 2;
            this.btnn2.Text = "2";
            this.btnn2.UseVisualStyleBackColor = false;
            this.btnn2.Click += new System.EventHandler(this.btnn2_Click);
            // 
            // btnn3
            // 
            this.btnn3.BackColor = System.Drawing.Color.Transparent;
            this.btnn3.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnn3.Location = new System.Drawing.Point(207, 34);
            this.btnn3.Margin = new System.Windows.Forms.Padding(4);
            this.btnn3.Name = "btnn3";
            this.btnn3.Size = new System.Drawing.Size(73, 53);
            this.btnn3.TabIndex = 3;
            this.btnn3.Text = "3";
            this.btnn3.UseVisualStyleBackColor = false;
            this.btnn3.Click += new System.EventHandler(this.btnn3_Click);
            // 
            // btnn6
            // 
            this.btnn6.BackColor = System.Drawing.Color.Transparent;
            this.btnn6.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnn6.Location = new System.Drawing.Point(207, 94);
            this.btnn6.Margin = new System.Windows.Forms.Padding(4);
            this.btnn6.Name = "btnn6";
            this.btnn6.Size = new System.Drawing.Size(73, 53);
            this.btnn6.TabIndex = 6;
            this.btnn6.Text = "6";
            this.btnn6.UseVisualStyleBackColor = false;
            this.btnn6.Click += new System.EventHandler(this.btnn6_Click);
            // 
            // btnn5
            // 
            this.btnn5.BackColor = System.Drawing.Color.Transparent;
            this.btnn5.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnn5.Location = new System.Drawing.Point(126, 94);
            this.btnn5.Margin = new System.Windows.Forms.Padding(4);
            this.btnn5.Name = "btnn5";
            this.btnn5.Size = new System.Drawing.Size(73, 53);
            this.btnn5.TabIndex = 5;
            this.btnn5.Text = "5";
            this.btnn5.UseVisualStyleBackColor = false;
            this.btnn5.Click += new System.EventHandler(this.btnn5_Click);
            // 
            // btnn4
            // 
            this.btnn4.BackColor = System.Drawing.Color.Transparent;
            this.btnn4.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnn4.Location = new System.Drawing.Point(45, 94);
            this.btnn4.Margin = new System.Windows.Forms.Padding(4);
            this.btnn4.Name = "btnn4";
            this.btnn4.Size = new System.Drawing.Size(73, 53);
            this.btnn4.TabIndex = 4;
            this.btnn4.Text = "4";
            this.btnn4.UseVisualStyleBackColor = false;
            this.btnn4.Click += new System.EventHandler(this.btnn4_Click);
            // 
            // btnn9
            // 
            this.btnn9.BackColor = System.Drawing.Color.Transparent;
            this.btnn9.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnn9.Location = new System.Drawing.Point(207, 154);
            this.btnn9.Margin = new System.Windows.Forms.Padding(4);
            this.btnn9.Name = "btnn9";
            this.btnn9.Size = new System.Drawing.Size(73, 53);
            this.btnn9.TabIndex = 9;
            this.btnn9.Text = "9";
            this.btnn9.UseVisualStyleBackColor = false;
            this.btnn9.Click += new System.EventHandler(this.btnn9_Click);
            // 
            // btnn8
            // 
            this.btnn8.BackColor = System.Drawing.Color.Transparent;
            this.btnn8.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnn8.Location = new System.Drawing.Point(126, 154);
            this.btnn8.Margin = new System.Windows.Forms.Padding(4);
            this.btnn8.Name = "btnn8";
            this.btnn8.Size = new System.Drawing.Size(73, 53);
            this.btnn8.TabIndex = 8;
            this.btnn8.Text = "8";
            this.btnn8.UseVisualStyleBackColor = false;
            this.btnn8.Click += new System.EventHandler(this.btnn8_Click);
            // 
            // btnn7
            // 
            this.btnn7.BackColor = System.Drawing.Color.Transparent;
            this.btnn7.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnn7.Location = new System.Drawing.Point(45, 154);
            this.btnn7.Margin = new System.Windows.Forms.Padding(4);
            this.btnn7.Name = "btnn7";
            this.btnn7.Size = new System.Drawing.Size(73, 53);
            this.btnn7.TabIndex = 7;
            this.btnn7.Text = "7";
            this.btnn7.UseVisualStyleBackColor = false;
            this.btnn7.Click += new System.EventHandler(this.btnn7_Click);
            // 
            // btnn0
            // 
            this.btnn0.BackColor = System.Drawing.Color.Transparent;
            this.btnn0.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnn0.Location = new System.Drawing.Point(45, 215);
            this.btnn0.Margin = new System.Windows.Forms.Padding(4);
            this.btnn0.Name = "btnn0";
            this.btnn0.Size = new System.Drawing.Size(73, 53);
            this.btnn0.TabIndex = 10;
            this.btnn0.Text = "0";
            this.btnn0.UseVisualStyleBackColor = false;
            this.btnn0.Click += new System.EventHandler(this.btnn0_Click);
            // 
            // btnx
            // 
            this.btnx.BackColor = System.Drawing.Color.Transparent;
            this.btnx.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnx.Location = new System.Drawing.Point(126, 215);
            this.btnx.Margin = new System.Windows.Forms.Padding(4);
            this.btnx.Name = "btnx";
            this.btnx.Size = new System.Drawing.Size(73, 53);
            this.btnx.TabIndex = 17;
            this.btnx.Text = "X";
            this.btnx.UseVisualStyleBackColor = false;
            this.btnx.Click += new System.EventHandler(this.btnx_Click);
            // 
            // BtnLimpiar
            // 
            this.BtnLimpiar.BackColor = System.Drawing.Color.Transparent;
            this.BtnLimpiar.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.BtnLimpiar.Location = new System.Drawing.Point(207, 215);
            this.BtnLimpiar.Margin = new System.Windows.Forms.Padding(4);
            this.BtnLimpiar.Name = "BtnLimpiar";
            this.BtnLimpiar.Size = new System.Drawing.Size(73, 53);
            this.BtnLimpiar.TabIndex = 18;
            this.BtnLimpiar.Text = "C";
            this.BtnLimpiar.UseVisualStyleBackColor = false;
            this.BtnLimpiar.Click += new System.EventHandler(this.BtnLimpiar_Click);
            // 
            // btnmultiplicar
            // 
            this.btnmultiplicar.BackColor = System.Drawing.Color.Transparent;
            this.btnmultiplicar.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmultiplicar.Location = new System.Drawing.Point(1046, 94);
            this.btnmultiplicar.Margin = new System.Windows.Forms.Padding(4);
            this.btnmultiplicar.Name = "btnmultiplicar";
            this.btnmultiplicar.Size = new System.Drawing.Size(73, 53);
            this.btnmultiplicar.TabIndex = 22;
            this.btnmultiplicar.Text = "*";
            this.btnmultiplicar.UseVisualStyleBackColor = false;
            this.btnmultiplicar.Click += new System.EventHandler(this.btnmultiplicar_Click);
            // 
            // btndivision
            // 
            this.btndivision.BackColor = System.Drawing.Color.Transparent;
            this.btndivision.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btndivision.Location = new System.Drawing.Point(965, 94);
            this.btndivision.Margin = new System.Windows.Forms.Padding(4);
            this.btndivision.Name = "btndivision";
            this.btndivision.Size = new System.Drawing.Size(73, 53);
            this.btndivision.TabIndex = 21;
            this.btndivision.Text = "/";
            this.btndivision.UseVisualStyleBackColor = false;
            this.btndivision.Click += new System.EventHandler(this.btndivision_Click);
            // 
            // btnmenos
            // 
            this.btnmenos.BackColor = System.Drawing.Color.Transparent;
            this.btnmenos.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnmenos.Location = new System.Drawing.Point(1046, 34);
            this.btnmenos.Margin = new System.Windows.Forms.Padding(4);
            this.btnmenos.Name = "btnmenos";
            this.btnmenos.Size = new System.Drawing.Size(73, 53);
            this.btnmenos.TabIndex = 20;
            this.btnmenos.Text = "-";
            this.btnmenos.UseVisualStyleBackColor = false;
            this.btnmenos.Click += new System.EventHandler(this.btnmenos_Click);
            // 
            // btnsuma
            // 
            this.btnsuma.BackColor = System.Drawing.Color.Transparent;
            this.btnsuma.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnsuma.Location = new System.Drawing.Point(965, 34);
            this.btnsuma.Margin = new System.Windows.Forms.Padding(4);
            this.btnsuma.Name = "btnsuma";
            this.btnsuma.Size = new System.Drawing.Size(73, 53);
            this.btnsuma.TabIndex = 19;
            this.btnsuma.Text = "+";
            this.btnsuma.UseVisualStyleBackColor = false;
            this.btnsuma.Click += new System.EventHandler(this.btnsuma_Click);
            // 
            // btnparentesis2
            // 
            this.btnparentesis2.BackColor = System.Drawing.Color.Transparent;
            this.btnparentesis2.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnparentesis2.Location = new System.Drawing.Point(1046, 154);
            this.btnparentesis2.Margin = new System.Windows.Forms.Padding(4);
            this.btnparentesis2.Name = "btnparentesis2";
            this.btnparentesis2.Size = new System.Drawing.Size(73, 53);
            this.btnparentesis2.TabIndex = 24;
            this.btnparentesis2.Text = ")";
            this.btnparentesis2.UseVisualStyleBackColor = false;
            this.btnparentesis2.Click += new System.EventHandler(this.btnparentesis2_Click);
            // 
            // btnparentensis1
            // 
            this.btnparentensis1.BackColor = System.Drawing.Color.Transparent;
            this.btnparentensis1.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnparentensis1.Location = new System.Drawing.Point(965, 154);
            this.btnparentensis1.Margin = new System.Windows.Forms.Padding(4);
            this.btnparentensis1.Name = "btnparentensis1";
            this.btnparentensis1.Size = new System.Drawing.Size(73, 53);
            this.btnparentensis1.TabIndex = 23;
            this.btnparentensis1.Text = "(";
            this.btnparentensis1.UseVisualStyleBackColor = false;
            this.btnparentensis1.Click += new System.EventHandler(this.btnparentensis1_Click);
            // 
            // btnelevar
            // 
            this.btnelevar.BackColor = System.Drawing.Color.Transparent;
            this.btnelevar.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnelevar.Location = new System.Drawing.Point(965, 215);
            this.btnelevar.Margin = new System.Windows.Forms.Padding(4);
            this.btnelevar.Name = "btnelevar";
            this.btnelevar.Size = new System.Drawing.Size(73, 53);
            this.btnelevar.TabIndex = 25;
            this.btnelevar.Text = "^";
            this.btnelevar.UseVisualStyleBackColor = false;
            this.btnelevar.Click += new System.EventHandler(this.btnelevar_Click);
            // 
            // btne
            // 
            this.btne.BackColor = System.Drawing.Color.Transparent;
            this.btne.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btne.Location = new System.Drawing.Point(1046, 215);
            this.btne.Margin = new System.Windows.Forms.Padding(4);
            this.btne.Name = "btne";
            this.btne.Size = new System.Drawing.Size(73, 53);
            this.btne.TabIndex = 40;
            this.btne.Text = "e";
            this.btne.UseVisualStyleBackColor = false;
            this.btne.Click += new System.EventHandler(this.btne_Click);
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9});
            this.dataTable1.TableName = "Table1";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "IT";
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "A";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "B";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "PM";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "F(a)";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "F(Pm)";
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "F(a)*F(pm)";
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "Error";
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "Criterio De Parada";
            // 
            // dataSet2
            // 
            this.dataSet2.DataSetName = "NewDataSet";
            this.dataSet2.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable2});
            // 
            // dataTable2
            // 
            this.dataTable2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn13,
            this.dataColumn14});
            this.dataTable2.TableName = "Table1";
            // 
            // dataColumn10
            // 
            this.dataColumn10.Caption = "A";
            this.dataColumn10.ColumnName = "A";
            // 
            // dataColumn11
            // 
            this.dataColumn11.Caption = "B";
            this.dataColumn11.ColumnName = "B";
            // 
            // dataColumn12
            // 
            this.dataColumn12.Caption = "F(a)";
            this.dataColumn12.ColumnName = "F(a)";
            // 
            // dataColumn13
            // 
            this.dataColumn13.Caption = "F(b)";
            this.dataColumn13.ColumnName = "F(b)";
            // 
            // dataColumn14
            // 
            this.dataColumn14.Caption = "F(a*b)";
            this.dataColumn14.ColumnName = "F(a*b)";
            // 
            // txterror
            // 
            this.txterror.Location = new System.Drawing.Point(527, 232);
            this.txterror.Margin = new System.Windows.Forms.Padding(4);
            this.txterror.Name = "txterror";
            this.txterror.Size = new System.Drawing.Size(153, 22);
            this.txterror.TabIndex = 47;
            this.txterror.TextChanged += new System.EventHandler(this.txterror_TextChanged);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(566, 171);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(81, 36);
            this.label4.TabIndex = 46;
            this.label4.Text = "Error";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(617, 56);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(30, 31);
            this.label3.TabIndex = 45;
            this.label3.Text = "b";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(340, 56);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(28, 31);
            this.label2.TabIndex = 44;
            this.label2.Text = "a";
            // 
            // txtintervalo2
            // 
            this.txtintervalo2.Location = new System.Drawing.Point(668, 65);
            this.txtintervalo2.Margin = new System.Windows.Forms.Padding(4);
            this.txtintervalo2.Name = "txtintervalo2";
            this.txtintervalo2.Size = new System.Drawing.Size(153, 22);
            this.txtintervalo2.TabIndex = 43;
            // 
            // txtintervalo1
            // 
            this.txtintervalo1.Location = new System.Drawing.Point(376, 65);
            this.txtintervalo1.Margin = new System.Windows.Forms.Padding(4);
            this.txtintervalo1.Name = "txtintervalo1";
            this.txtintervalo1.Size = new System.Drawing.Size(153, 22);
            this.txtintervalo1.TabIndex = 42;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(496, 9);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 36);
            this.label1.TabIndex = 41;
            this.label1.Text = "Intervalos";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(459, 128);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(31, 31);
            this.label6.TabIndex = 50;
            this.label6.Text = "X";
            this.label6.Click += new System.EventHandler(this.label6_Click);
            // 
            // txtx0
            // 
            this.txtx0.Location = new System.Drawing.Point(527, 135);
            this.txtx0.Margin = new System.Windows.Forms.Padding(4);
            this.txtx0.Name = "txtx0";
            this.txtx0.Size = new System.Drawing.Size(153, 22);
            this.txtx0.TabIndex = 49;
            this.txtx0.TextChanged += new System.EventHandler(this.txtx0_TextChanged);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft YaHei", 7.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(498, 138);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(18, 19);
            this.label7.TabIndex = 51;
            this.label7.Text = "0";
            // 
            // dataSet3
            // 
            this.dataSet3.DataSetName = "NewDataSet";
            this.dataSet3.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable3});
            // 
            // dataTable3
            // 
            this.dataTable3.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn15,
            this.dataColumn16,
            this.dataColumn17,
            this.dataColumn18,
            this.dataColumn19});
            this.dataTable3.TableName = "Table1";
            // 
            // dataColumn15
            // 
            this.dataColumn15.Caption = "IT";
            this.dataColumn15.ColumnName = "IT";
            // 
            // dataColumn16
            // 
            this.dataColumn16.Caption = "Xi + 1";
            this.dataColumn16.ColumnName = "Xi + 1";
            // 
            // dataColumn17
            // 
            this.dataColumn17.Caption = "F(Xi)";
            this.dataColumn17.ColumnName = "F(Xi)";
            // 
            // dataColumn18
            // 
            this.dataColumn18.Caption = "Error";
            this.dataColumn18.ColumnName = "Error";
            // 
            // dataColumn19
            // 
            this.dataColumn19.Caption = "Criterio de Parada";
            this.dataColumn19.ColumnName = "Criterio de Parada";
            // 
            // dataSet4
            // 
            this.dataSet4.DataSetName = "NewDataSet";
            this.dataSet4.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable4});
            // 
            // dataTable4
            // 
            this.dataTable4.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn20,
            this.dataColumn21,
            this.dataColumn22,
            this.dataColumn23,
            this.dataColumn24,
            this.dataColumn25});
            this.dataTable4.TableName = "Raphson";
            // 
            // dataColumn20
            // 
            this.dataColumn20.Caption = "IT";
            this.dataColumn20.ColumnName = "IT";
            // 
            // dataColumn21
            // 
            this.dataColumn21.ColumnName = "X+1";
            // 
            // dataColumn22
            // 
            this.dataColumn22.ColumnName = "F(x)";
            // 
            // dataColumn23
            // 
            this.dataColumn23.ColumnName = "F\'(x)";
            // 
            // dataColumn24
            // 
            this.dataColumn24.ColumnName = "Error";
            // 
            // dataColumn25
            // 
            this.dataColumn25.ColumnName = "Criterio de Parada";
            // 
            // dataSet5
            // 
            this.dataSet5.DataSetName = "NewDataSet";
            this.dataSet5.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable5});
            // 
            // dataTable5
            // 
            this.dataTable5.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn26,
            this.dataColumn27,
            this.dataColumn28,
            this.dataColumn29,
            this.dataColumn30,
            this.dataColumn31,
            this.dataColumn32,
            this.dataColumn33,
            this.dataColumn34,
            this.dataColumn35});
            this.dataTable5.TableName = "Table1";
            // 
            // dataColumn26
            // 
            this.dataColumn26.ColumnName = "IT";
            // 
            // dataColumn27
            // 
            this.dataColumn27.ColumnName = "Xi";
            // 
            // dataColumn28
            // 
            this.dataColumn28.ColumnName = "Xs";
            // 
            // dataColumn29
            // 
            this.dataColumn29.ColumnName = "Xr";
            // 
            // dataColumn30
            // 
            this.dataColumn30.ColumnName = "F(Xi)";
            // 
            // dataColumn31
            // 
            this.dataColumn31.ColumnName = "F(Xs)";
            // 
            // dataColumn32
            // 
            this.dataColumn32.ColumnName = "F(Xr)";
            // 
            // dataColumn33
            // 
            this.dataColumn33.ColumnName = "F(Xi*Xr)";
            // 
            // dataColumn34
            // 
            this.dataColumn34.ColumnName = "Error";
            // 
            // dataColumn35
            // 
            this.dataColumn35.ColumnName = "C. Parada";
            // 
            // contextMenuStrip1
            // 
            this.contextMenuStrip1.ImageScalingSize = new System.Drawing.Size(20, 20);
            this.contextMenuStrip1.Name = "contextMenuStrip1";
            this.contextMenuStrip1.Size = new System.Drawing.Size(61, 4);
            // 
            // TxtPantallaCalculo
            // 
            this.TxtPantallaCalculo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.TxtPantallaCalculo.Font = new System.Drawing.Font("Microsoft YaHei", 30F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TxtPantallaCalculo.Location = new System.Drawing.Point(26, 533);
            this.TxtPantallaCalculo.Margin = new System.Windows.Forms.Padding(4);
            this.TxtPantallaCalculo.Multiline = true;
            this.TxtPantallaCalculo.Name = "TxtPantallaCalculo";
            this.TxtPantallaCalculo.Size = new System.Drawing.Size(1074, 142);
            this.TxtPantallaCalculo.TabIndex = 52;
            // 
            // btncalcular
            // 
            this.btncalcular.Font = new System.Drawing.Font("Microsoft YaHei", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncalcular.Location = new System.Drawing.Point(1109, 557);
            this.btncalcular.Margin = new System.Windows.Forms.Padding(4);
            this.btncalcular.Name = "btncalcular";
            this.btncalcular.Size = new System.Drawing.Size(161, 48);
            this.btncalcular.TabIndex = 61;
            this.btncalcular.Text = "Calcular";
            this.btncalcular.UseVisualStyleBackColor = true;
            this.btncalcular.Click += new System.EventHandler(this.btncalcular_Click);
            // 
            // btnln
            // 
            this.btnln.BackColor = System.Drawing.Color.Transparent;
            this.btnln.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnln.Location = new System.Drawing.Point(538, 416);
            this.btnln.Margin = new System.Windows.Forms.Padding(4);
            this.btnln.Name = "btnln";
            this.btnln.Size = new System.Drawing.Size(161, 53);
            this.btnln.TabIndex = 60;
            this.btnln.Text = "Ln(";
            this.btnln.UseVisualStyleBackColor = false;
            this.btnln.Click += new System.EventHandler(this.btnln_Click_1);
            // 
            // btnlog
            // 
            this.btnlog.BackColor = System.Drawing.Color.Transparent;
            this.btnlog.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnlog.Location = new System.Drawing.Point(289, 416);
            this.btnlog.Margin = new System.Windows.Forms.Padding(4);
            this.btnlog.Name = "btnlog";
            this.btnlog.Size = new System.Drawing.Size(161, 53);
            this.btnlog.TabIndex = 59;
            this.btnlog.Text = "Log(";
            this.btnlog.UseVisualStyleBackColor = false;
            this.btnlog.Click += new System.EventHandler(this.btnlog_Click_1);
            // 
            // btncotangente
            // 
            this.btncotangente.BackColor = System.Drawing.Color.Transparent;
            this.btncotangente.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btncotangente.Location = new System.Drawing.Point(1021, 416);
            this.btncotangente.Margin = new System.Windows.Forms.Padding(4);
            this.btncotangente.Name = "btncotangente";
            this.btncotangente.Size = new System.Drawing.Size(164, 53);
            this.btncotangente.TabIndex = 58;
            this.btncotangente.Text = "COTANGENTE";
            this.btncotangente.UseVisualStyleBackColor = false;
            this.btncotangente.Click += new System.EventHandler(this.btncotangente_Click_1);
            // 
            // Btnscosecante
            // 
            this.Btnscosecante.BackColor = System.Drawing.Color.Transparent;
            this.Btnscosecante.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btnscosecante.Location = new System.Drawing.Point(781, 416);
            this.Btnscosecante.Margin = new System.Windows.Forms.Padding(4);
            this.Btnscosecante.Name = "Btnscosecante";
            this.Btnscosecante.Size = new System.Drawing.Size(161, 53);
            this.Btnscosecante.TabIndex = 57;
            this.Btnscosecante.Text = "SECANTE";
            this.Btnscosecante.UseVisualStyleBackColor = false;
            this.Btnscosecante.Click += new System.EventHandler(this.Btnscosecante_Click);
            // 
            // Btnsecante
            // 
            this.Btnsecante.BackColor = System.Drawing.Color.Transparent;
            this.Btnsecante.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btnsecante.Location = new System.Drawing.Point(26, 416);
            this.Btnsecante.Margin = new System.Windows.Forms.Padding(4);
            this.Btnsecante.Name = "Btnsecante";
            this.Btnsecante.Size = new System.Drawing.Size(161, 53);
            this.Btnsecante.TabIndex = 56;
            this.Btnsecante.Text = "COSECANTE";
            this.Btnsecante.UseVisualStyleBackColor = false;
            this.Btnsecante.Click += new System.EventHandler(this.Btnsecante_Click);
            // 
            // btntangente
            // 
            this.btntangente.BackColor = System.Drawing.Color.Transparent;
            this.btntangente.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btntangente.Location = new System.Drawing.Point(936, 324);
            this.btntangente.Margin = new System.Windows.Forms.Padding(4);
            this.btntangente.Name = "btntangente";
            this.btntangente.Size = new System.Drawing.Size(164, 53);
            this.btntangente.TabIndex = 55;
            this.btntangente.Text = "TANGENTE";
            this.btntangente.UseVisualStyleBackColor = false;
            this.btntangente.Click += new System.EventHandler(this.btntangente_Click_1);
            // 
            // Btnscoseno
            // 
            this.Btnscoseno.BackColor = System.Drawing.Color.Transparent;
            this.Btnscoseno.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btnscoseno.Location = new System.Drawing.Point(376, 324);
            this.Btnscoseno.Margin = new System.Windows.Forms.Padding(4);
            this.Btnscoseno.Name = "Btnscoseno";
            this.Btnscoseno.Size = new System.Drawing.Size(161, 53);
            this.Btnscoseno.TabIndex = 54;
            this.Btnscoseno.Text = "COSENO";
            this.Btnscoseno.UseVisualStyleBackColor = false;
            this.Btnscoseno.Click += new System.EventHandler(this.Btnscoseno_Click);
            // 
            // Btnseno
            // 
            this.Btnseno.BackColor = System.Drawing.Color.Transparent;
            this.Btnseno.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Btnseno.Location = new System.Drawing.Point(106, 324);
            this.Btnseno.Margin = new System.Windows.Forms.Padding(4);
            this.Btnseno.Name = "Btnseno";
            this.Btnseno.Size = new System.Drawing.Size(161, 53);
            this.Btnseno.TabIndex = 53;
            this.Btnseno.Text = "SENO";
            this.Btnseno.UseVisualStyleBackColor = false;
            this.Btnseno.Click += new System.EventHandler(this.Btnseno_Click_1);
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.Transparent;
            this.button1.Font = new System.Drawing.Font("Microsoft YaHei", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(668, 324);
            this.button1.Margin = new System.Windows.Forms.Padding(4);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(161, 53);
            this.button1.TabIndex = 62;
            this.button1.Text = "Sqrt (Raiz)";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtxo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1283, 762);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.btncalcular);
            this.Controls.Add(this.btnln);
            this.Controls.Add(this.btnlog);
            this.Controls.Add(this.btncotangente);
            this.Controls.Add(this.Btnscosecante);
            this.Controls.Add(this.Btnsecante);
            this.Controls.Add(this.btntangente);
            this.Controls.Add(this.Btnscoseno);
            this.Controls.Add(this.Btnseno);
            this.Controls.Add(this.TxtPantallaCalculo);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtx0);
            this.Controls.Add(this.txterror);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtintervalo2);
            this.Controls.Add(this.txtintervalo1);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btne);
            this.Controls.Add(this.btnelevar);
            this.Controls.Add(this.btnparentesis2);
            this.Controls.Add(this.btnparentensis1);
            this.Controls.Add(this.btnmultiplicar);
            this.Controls.Add(this.btndivision);
            this.Controls.Add(this.btnmenos);
            this.Controls.Add(this.btnsuma);
            this.Controls.Add(this.BtnLimpiar);
            this.Controls.Add(this.btnx);
            this.Controls.Add(this.btnn0);
            this.Controls.Add(this.btnn9);
            this.Controls.Add(this.btnn8);
            this.Controls.Add(this.btnn7);
            this.Controls.Add(this.btnn6);
            this.Controls.Add(this.btnn5);
            this.Controls.Add(this.btnn4);
            this.Controls.Add(this.btnn3);
            this.Controls.Add(this.btnn2);
            this.Controls.Add(this.btnn1);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "txtxo";
            this.Text = "Form2";
            this.Load += new System.EventHandler(this.Form2_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable3)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable4)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet5)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable5)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button btnn1;
        private System.Windows.Forms.Button btnn2;
        private System.Windows.Forms.Button btnn3;
        private System.Windows.Forms.Button btnn6;
        private System.Windows.Forms.Button btnn5;
        private System.Windows.Forms.Button btnn4;
        private System.Windows.Forms.Button btnn9;
        private System.Windows.Forms.Button btnn8;
        private System.Windows.Forms.Button btnn7;
        private System.Windows.Forms.Button btnn0;
        private System.Windows.Forms.Button btnx;
        private System.Windows.Forms.Button BtnLimpiar;
        private System.Windows.Forms.Button btnmultiplicar;
        private System.Windows.Forms.Button btndivision;
        private System.Windows.Forms.Button btnmenos;
        private System.Windows.Forms.Button btnsuma;
        private System.Windows.Forms.Button btnparentesis2;
        private System.Windows.Forms.Button btnparentensis1;
        private System.Windows.Forms.Button btnelevar;
        private System.Windows.Forms.Button btne;
        private System.Data.DataSet dataSet1;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private System.Data.DataSet dataSet2;
        private System.Data.DataTable dataTable2;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private System.Data.DataColumn dataColumn13;
        private System.Data.DataColumn dataColumn14;
        private System.Windows.Forms.TextBox txterror;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtintervalo2;
        private System.Windows.Forms.TextBox txtintervalo1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtx0;
        private System.Windows.Forms.Label label7;
        private System.Data.DataSet dataSet3;
        private System.Data.DataTable dataTable3;
        private System.Data.DataColumn dataColumn15;
        private System.Data.DataColumn dataColumn16;
        private System.Data.DataColumn dataColumn17;
        private System.Data.DataColumn dataColumn18;
        private System.Data.DataColumn dataColumn19;
        private System.Data.DataSet dataSet4;
        private System.Data.DataTable dataTable4;
        private System.Data.DataColumn dataColumn20;
        private System.Data.DataColumn dataColumn21;
        private System.Data.DataColumn dataColumn22;
        private System.Data.DataColumn dataColumn23;
        private System.Data.DataColumn dataColumn24;
        private System.Data.DataColumn dataColumn25;
        private System.Data.DataSet dataSet5;
        private System.Data.DataTable dataTable5;
        private System.Data.DataColumn dataColumn26;
        private System.Data.DataColumn dataColumn27;
        private System.Data.DataColumn dataColumn28;
        private System.Data.DataColumn dataColumn29;
        private System.Data.DataColumn dataColumn30;
        private System.Data.DataColumn dataColumn31;
        private System.Data.DataColumn dataColumn32;
        private System.Data.DataColumn dataColumn33;
        private System.Data.DataColumn dataColumn34;
        private System.Data.DataColumn dataColumn35;
        private System.Windows.Forms.ContextMenuStrip contextMenuStrip1;
        private System.Windows.Forms.TextBox TxtPantallaCalculo;
        private System.Windows.Forms.Button btncalcular;
        private System.Windows.Forms.Button btnln;
        private System.Windows.Forms.Button btnlog;
        private System.Windows.Forms.Button btncotangente;
        private System.Windows.Forms.Button Btnscosecante;
        private System.Windows.Forms.Button Btnsecante;
        private System.Windows.Forms.Button btntangente;
        private System.Windows.Forms.Button Btnscoseno;
        private System.Windows.Forms.Button Btnseno;
        private System.Windows.Forms.Button button1;
    }
}