﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ProyectoMN
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }

        private void Form4_Load(object sender, EventArgs e)
        {





        }


        public void eliminaciongaussiana()
        {
            double a11 = Convert.ToDouble(textBox1.Text);
            double a12 = Convert.ToDouble(textBox2.Text);
            double a13 = Convert.ToDouble(textBox3.Text); ;
            double a14 = Convert.ToDouble(textBox18.Text);
            double r1 = Convert.ToDouble(textBox4.Text); ;
            double b11 = Convert.ToDouble(textBox5.Text); ;
            double b12 = Convert.ToDouble(textBox6.Text); ;
            double b13 = Convert.ToDouble(textBox7.Text); ;
            double b24 = Convert.ToDouble(textBox19.Text);
            double r2 = Convert.ToDouble(textBox8.Text); ;
            double c11 = Convert.ToDouble(textBox9.Text); ;
            double c12 = Convert.ToDouble(textBox10.Text); ;
            double c13 = Convert.ToDouble(textBox11.Text); ;
            double c34 = Convert.ToDouble(textBox20.Text);
            double r3 = Convert.ToDouble(textBox12.Text); ;
            double d41 = Convert.ToDouble(textBox13.Text);
            double d42 = Convert.ToDouble(textBox14.Text);
            double d43 = Convert.ToDouble(textBox15.Text);
            double d44 = Convert.ToDouble(textBox16.Text);
            double r4 = Convert.ToDouble(textBox17.Text);


            //Fila uno

            a12 = a12 / a11;
            a13 = a13 / a11;
            r1 = r1 / a11;
            a11 = a11 / a11;

            if (a11 < 0)
            {
                a12 = a12 / -a11;
                a13 = a13 / -a11;
                r1 = r1 / -a11;
                a11 = a11 / -a11;
            }



            //Fila dos 


            b12 = b12 + (-b11 * a12);
            b13 = b13 + (-b11 * a13);
            r2 = r2 + (-b11 * r1);
            b11 = b11 + (-b11 * a11);


            b13 = b13 / b12;
            r2 = r2 / b12;
            b12 = b12 / b12;

            if (b12 < 0)
            {
                b13 = b13 / -b12;
                r2 = r2 / -b12;
                b12 = b12 / -b12;
            }

            //Fila tres



            c12 = c12 + (-c11 * a12);
            c13 = c13 + (-c11 * a13);
            r3 = r3 + (-c11 * r1);
            c11 = c11 + (-c11 * a11);



            r3 = r3 + (-c12 * r2);
            c13 = c13 + (-c12 * b13);
            c12 = c12 + (-c12 * b12);


            r3 = r3 / c13;
            c13 = c13 / c13;


            if (c13 < 0)
            {

                r3 = r3 / -c13;
                c13 = b13 / -c13;

            }

            //Fila cuatro

            d42 = d42 + (-d41 * a12);
            d43 = d43 + (-d41 * a13);
            d44 = d44 + (-d41 * a14);
            r4 = r4 + (-d41 * r1);
            d41 = d41 + (-d41 * a11);

            r4 = r4 + (-d42 * r2);
            d44 = d44 + (-d42 * b24);
            d43 = d43 + (-d42 * b13);
            d42 = d42 + (-d42 * b12);

            r4 = r4 + (-d43 * r3);
            d44 = d44 + (-d43 * c34);
            d43 = d43 + (-d43 * c13);

            r4 = r4 + (-d44 * r4);
            d44 = d44 + (-d44 * d44);

            r4 = r4/-d44;

            if(d44 < 0)
            {
                r4 = r4/-d44;
            }
            
             
           a12=  Math.Round(a12, 2);
           a13=  Math.Round(a13, 2);
           r1 = Math.Round(r1, 2);
           b13 =Math.Round(b13, 2);
           r2= Math.Round(r2, 2);
           r3= Math.Round(r3, 2);



            //Mostrar resultados


            label5.Text = Convert.ToString(a11);
            label6.Text = Convert.ToString(a12);
            label7.Text = Convert.ToString(a13);
            label8.Text = Convert.ToString(r1);


            label12.Text = Convert.ToString(b11);
            label11.Text = Convert.ToString(b12);
            label10.Text = Convert.ToString(b13);
            label9.Text = Convert.ToString(r2);


            label16.Text = Convert.ToString(c11);
            label15.Text = Convert.ToString(c12);
            label14.Text = Convert.ToString(c13);
            label13.Text = Convert.ToString(r3);


            double resultado1;
            double resultado2;
            double resultado3;
            double resultado4;

            resultado4 = r4;
            resultado3 = r3 - (c34 * resultado4);
            resultado2 = r2 - (b13 * resultado3) - (b24 * resultado4);
            resultado1 = r1 - (a12 * resultado2) - (a13 * resultado3) - (a14 * resultado4);
             

            txtresultado1.Text = Convert.ToString(Math.Round(resultado1, 2));
            txtresultado2.Text = Convert.ToString(Math.Round(resultado2, 2));
            txtresultado3.Text = Convert.ToString(Math.Round(resultado3, 2));
            txtresultado4.Text = Convert.ToString(Math.Round(resultado4, 2));





        }



        public void Convergencias()
        {
            double a11 = Convert.ToDouble(textBox1.Text);
            double a12 = Convert.ToDouble(textBox2.Text); ;
            double a13 = Convert.ToDouble(textBox3.Text); ;
            double r1 = Convert.ToDouble(textBox4.Text); ;
            double b11 = Convert.ToDouble(textBox5.Text); ;
            double b12 = Convert.ToDouble(textBox6.Text); ;
            double b13 = Convert.ToDouble(textBox7.Text); ;
            double r2 = Convert.ToDouble(textBox8.Text); ;
            double c11 = Convert.ToDouble(textBox9.Text); ;
            double c12 = Convert.ToDouble(textBox10.Text); ;
            double c13 = Convert.ToDouble(textBox11.Text); ;
            double r3 = Convert.ToDouble(textBox12.Text); ;
            double d41 = Convert.ToDouble(textBox13.Text);;
            double d42 = Convert.ToDouble(textBox14);;
            double d43 = Convert.ToDouble(textBox15);;
            double d44 = Convert.ToDouble(textBox16);;
            double r4 = Convert.ToDouble(textBox17);;

            String Convergencia1 = "No converge";
            String Convergencia2 = "No converge";
            String Convergencia3 = "No converge";
            String convergencia4 = "No converge";

#pragma warning disable CS0219 // La variable 'condicion1' está asignada pero su valor nunca se usa
            bool condicion1 = false;
#pragma warning restore CS0219 // La variable 'condicion1' está asignada pero su valor nunca se usa


            // Convergencias

            if (Math.Abs(a11) >= (Math.Abs(a12) + Math.Abs(a13)))
            {
                Convergencia1 = " Converge";
            }
            else
            {
                Convergencia1 = "No converge";
            }

            if (Math.Abs(b12) >= (Math.Abs(b11) + Math.Abs(b13)))
            {

                Convergencia2 = " Converge";
            }
            else
            {
                Convergencia3 = "No converge";
            }

            if (Math.Abs(c13) >= (Math.Abs(c11) + Math.Abs(c12)))
            {

                Convergencia3 = " Converge";
            }
            else
            {
                Convergencia3 = "No converge";
            }

            if (Math.Abs((d44) >= (Math.Abs(d41) + Math.Abs(d42) + Math.Abs(d43)))
            {
                convergencia4 = "Converge";
            }
            else
            {
                convergencia4 = "no converge";
            }



            DataTable dtconvergencia = dataSet1.Tables[0];
            DataRow drconvergencia = dataSet1.Tables[0].NewRow();
            drconvergencia[0] = Convert.ToString(a11);
            drconvergencia[1] = Convert.ToString(a12);
            drconvergencia[2] = Convert.ToString(a13);
            drconvergencia[3] = Convert.ToString(r1);
            drconvergencia[4] = Convergencia1;
            dtconvergencia.Rows.Add(drconvergencia);

            drconvergencia = dataSet1.Tables[0].NewRow();
            drconvergencia[0] = Convert.ToString(b11);
            drconvergencia[1] = Convert.ToString(b12);
            drconvergencia[2] = Convert.ToString(b13);
            drconvergencia[3] = Convert.ToString(r2);
            drconvergencia[4] = Convergencia2;
            dtconvergencia.Rows.Add(drconvergencia);

            drconvergencia = dataSet1.Tables[0].NewRow();
            drconvergencia[0] = Convert.ToString(c11);
            drconvergencia[1] = Convert.ToString(c12);
            drconvergencia[2] = Convert.ToString(c13);
            drconvergencia[3] = Convert.ToString(r3);
            drconvergencia[4] = Convergencia3;
            dtconvergencia.Rows.Add(drconvergencia);

             drconvergencia = dataSet1.Tables[0].NewRow();
            drconvergencia[0] = Convert.ToString(d41);
            drconvergencia[1] = Convert.ToString(d42);
            drconvergencia[2] = Convert.ToString(d43);
            drconvergencia[3] = Convert.ToString(d44);
            drconvergencia[4] = Convergencia4;
            dtconvergencia.Rows.Add(drconvergencia);




            dgvconvergencia.DataSource = dataSet1.Tables[0];


        }

        public void Jacobi()
        {
            

            double a11 = Convert.ToDouble(textBox1.Text);
            double a12 = Convert.ToDouble(textBox2.Text); ;
            double a13 = Convert.ToDouble(textBox3.Text); ;
            double r1 = Convert.ToDouble(textBox4.Text); ;
            double b11 = Convert.ToDouble(textBox5.Text); ;
            double b12 = Convert.ToDouble(textBox6.Text); ;
            double b13 = Convert.ToDouble(textBox7.Text); ;
            double r2 = Convert.ToDouble(textBox8.Text); ;
            double c11 = Convert.ToDouble(textBox9.Text); ;
            double c12 = Convert.ToDouble(textBox10.Text); ;
            double c13 = Convert.ToDouble(textBox11.Text); ;
            double r3 = Convert.ToDouble(textBox12.Text); ;
            double d41 = Convert.ToDouble(textbox13.Text);;
            double d42 = Convert.ToDouble(textBox14.Text);;
            double d43 = Convert.ToDouble(textBox15.Text);;
            double d44 = Convert.ToDouble(textBox16.Text);;

            double x1;
            double x2;
            double x3;
            double x4;

            double it = 0; 

            double xpos1 = 0;
            double xpos2 = 0;
            double xpos3 = 0;
            double xpos4 = 0;

            double tolerancia = Convert.ToDouble( txterror.Text);

            double error1=0;
            double error2=0;
            double error3=0;
            double error4=0;
            bool condicion1 = false;

            String c1 = "Falso";
            String c2 = "Falso";
            String c3 = "Falso";
            String c4 = "Falso";

            DataTable dtjacobi = dataSet2.Tables[0];
            DataRow drjacobi = dataSet2.Tables[0].NewRow();
            drjacobi[0] = Convert.ToString(it);
            drjacobi[1] = Convert.ToString(xpos1);
            drjacobi[2] = Convert.ToString(xpos2);
            drjacobi[3] = Convert.ToString(xpos3);
            drjacobi[11] = Convert.ToString(xpos4);
            drjacobi[4] = Convert.ToString(error1);
            drjacobi[5] = Convert.ToString(error2);
            drjacobi[6] = Convert.ToString(error3);
            drjacobi[12] = Convert.ToString(error4);
            drjacobi[7] = c1;
            drjacobi[8] = c2;
            drjacobi[9] = c3;
            drjacobi[13] = c4;
            drjacobi[10] = Convert.ToString(tolerancia);
            dtjacobi.Rows.Add(drjacobi);


            while (condicion1 == false) 
            {                
                it = it + 1;

            x1 = (r1 - (a14 * xpos4) - (a13 * xpos3) - (a12 * xpos2)) / a11;
            x2 = (r2 - (b24 * xpos4) - (b13 * xpos3) - (b11 * x1)) / b12;
            x3 = (r3 - (c34 * xpos4) - (c12 * x2) - (c11 * x1)) / c13;
            x4 = (r4 - (d43 * x3) - (d42*x2) - (d41*x1))/d44;

            error1 = Math.Abs(x1 - xpos1);
            error2 = Math.Abs(x2 - xpos2);
            error3 = Math.Abs(x3 - xpos3);
            error4 = Math.Abs(x4 - xpos4);


            if (error1 < tolerancia)
            {
                c1 = "Verdadero";
            }
            else
            {
                c1 = "Falso";
            }

            if (error2 < tolerancia)
            {
                c2 = "Verdadero";
            }
            else
            {
                c2 = "Falso";
            }

            if (error3 < tolerancia)
            {
                c3 = "Verdadero";
            }
            else
            {
                c3 = "Falso";
            }
            if(error4 < tolerancia)
            {
                    c4 = "Verdadero";
            }
            else
            {
                    c4 = "Falso";
            }


        

            drjacobi = dataSet2.Tables[0].NewRow();
            drjacobi[0] = Convert.ToString(it);
            drjacobi[1] = Convert.ToString(x1);
            drjacobi[2] = Convert.ToString(x2);
            drjacobi[3] = Convert.ToString(x3);
            drjacobi[11] = Convert.ToString(x4);
            drjacobi[4] = Convert.ToString(error1);
            drjacobi[5] = Convert.ToString(error2);
            drjacobi[6] = Convert.ToString(error3);
            drjacobi[12] = Convert.ToString(error4);
            drjacobi[7] = c1;
            drjacobi[8] = c2;
            drjacobi[9] = c3;
            drjacobi[13] = c4;
            drjacobi[10] = Convert.ToString(tolerancia);
            dtjacobi.Rows.Add(drjacobi);


            xpos1 = x1;
            xpos2 = x2;
            xpos3 = x3;
            xpos4 = x4;

                if (c1.Equals("Verdadero") && c2.Equals("Verdadero") && c3.Equals("Verdadero") && c4.Equals("Verdadero"))
                {
                    condicion1 = true;
                }


            }





        }


        public void Gaussseidel()
        {
            double a11 = Convert.ToDouble(textBox1.Text);
            double a12 = Convert.ToDouble(textBox2.Text); ;
            double a13 = Convert.ToDouble(textBox3.Text); ;
            double r1 = Convert.ToDouble(textBox4.Text); ;
            double b11 = Convert.ToDouble(textBox5.Text); ;
            double b12 = Convert.ToDouble(textBox6.Text); ;
            double b13 = Convert.ToDouble(textBox7.Text); ;
            double r2 = Convert.ToDouble(textBox8.Text); ;
            double c11 = Convert.ToDouble(textBox9.Text); ;
            double c12 = Convert.ToDouble(textBox10.Text); ;
            double c13 = Convert.ToDouble(textBox11.Text); ;
            double r3 = Convert.ToDouble(textBox12.Text); ;
            double d41 = Convert.ToDouble(textBox13.Text);;
            double d42 = Convert.ToDouble(textBox14.Text);;
            double d43 = Convert.ToDouble(textBox15.Text);;
            double d44 = Convert.ToDouble(textBox16.Text);;
            double r4 = Convert.ToDouble(textBox17.Text);;

            double x1;
            double x2;
            double x3;
            double x4;

            double it = 0;

            double xpos1 = 0;
            double xpos2 = 0;
            double xpos3 = 0;
            double xpos4 = 0;

            double tolerancia = Convert.ToDouble(txterror.Text);

            double error1 = 0;
            double error2 = 0;
            double error3 = 0;
            double error4 = 0;
            bool condicion1 = false;

            String c1 = "Falso";
            String c2 = "Falso";
            String c3 = "Falso";
            String c4 = "Falso";


    
            DataTable dtSeidel = dataSet3.Tables[0];
            DataRow drseidel = dataSet3.Tables[0].NewRow();
            drseidel[0] = Convert.ToString(it);
            drseidel[1] = Convert.ToString(xpos1);
            drseidel[2] = Convert.ToString(xpos2);
            drseidel[3] = Convert.ToString(xpos3);
            drseidel[11] = Convert.ToString(xpos4);
            drseidel[4] = Convert.ToString(error1);
            drseidel[5] = Convert.ToString(error2);
            drseidel[6] = Convert.ToString(error3);
            drseidel[12] = Convert.ToString(error4);
            drseidel[7] = c1;
            drseidel[8] = c2;
            drseidel[9] = c3;
            drseidel[13] = c4;
            drseidel[10] = Convert.ToString(tolerancia);
            dtSeidel.Rows.Add(drseidel);


            while (condicion1 == false)
            {
                it = it + 1;

                x1 = (r1 - (a14 * xpos4) - (a13 * xpos3) - (a12 * xpos2)) / a11;
                x2 = (r2 - (b24 * xpos4) - (b13 * xpos3) - (b11 * xpos1)) / b12;
                x3 = (r3 - (c34 * xpos4) - (c12 * xpos2) - (c11 * xpos1)) / c13;
                x4 = (r4 - (d43 * xpos3) - (d42 * xpos2) - (d41 * xpos1)) / d44;

                error1 = Math.Abs(x1 - xpos1);
                error2 = Math.Abs(x2 - xpos2);
                error3 = Math.Abs(x3 - xpos3);
                error4 = Math.Abs(x4 - xpos4);

                if (error1 < tolerancia)
                {
                    c1 = "Verdadero";
                }
                else
                {
                    c1 = "Falso";
                }

                if (error2 < tolerancia)
                {
                    c2 = "Verdadero";
                }
                else
                {
                    c2 = "Falso";
                }

                if (error3 < tolerancia)
                {
                    c3 = "Verdadero";
                }
                else
                {
                    c3 = "Falso";
                }
                if(error4 < tolerancia)
                {
                    c4 = "Verdadero";
                }
                else
                {
                    c4 = "Falso";
                }



                drseidel = dataSet3.Tables[0].NewRow();
                drseidel[0] = Convert.ToString(it);
                drseidel[1] = Convert.ToString(xpos1);
                drseidel[2] = Convert.ToString(xpos2);
                drseidel[3] = Convert.ToString(xpos3);
                drseidel[11] = Convert.ToString(xpos4);
                drseidel[4] = Convert.ToString(error1);
                drseidel[5] = Convert.ToString(error2);
                drseidel[6] = Convert.ToString(error3);
                drseidel[12] = Convert.ToString(error4);
                drseidel[7] = c1;
                drseidel[8] = c2;
                drseidel[9] = c3;
                drseidel[13] = c4;
                drseidel[10] = Convert.ToString(tolerancia);
                dtSeidel.Rows.Add(drseidel);



                xpos1 = x1;
                xpos2 = x2;
                xpos3 = x3;
                xpos4 = x4;

                if (c1.Equals("Verdadero") && c2.Equals("Verdadero") && c3.Equals("Verdadero") && c4.Equals("Verdadero")
                {
                    condicion1 = true;
                }


            }


        }



        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void Calcular_Click(object sender, EventArgs e)
        {
       
        }

        private void Calcular_Click_1(object sender, EventArgs e)
        {
            try{ 
            // LLamar al metodo de eliminacion.
            eliminaciongaussiana();

            //Limpiar el data set para que no existan datos anteriores.
            dataSet1.Clear();

            //LLamar a las convergencias.
            Convergencias(); }
             catch (System.IO.IOException ex)
            {
                MessageBox.Show(ex.Message);
            }


        }

        private void tabPage4_Click(object sender, EventArgs e)
        {

        }

        private void btnJacobi_Click(object sender, EventArgs e)
        {

            try
            {
                //Limpiar el data set de jacobi

                dataSet2.Clear();

                // Llamar a jacobi
                dgvjacobi.DataSource = dataSet2.Tables[0];
                Jacobi();
            }
            catch (System.IO.IOException ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void btnseidel_Click(object sender, EventArgs e)
        {
            try { 
            // Limpiar el data set de Seidel
            dataSet3.Clear();

            // LLamar Seidel
            dgvseidel.DataSource = dataSet3.Tables[0];
            Gaussseidel();
        }
             catch (System.IO.IOException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        private void tabPage1_Click(object sender, EventArgs e)
        {

        }
    }
}
