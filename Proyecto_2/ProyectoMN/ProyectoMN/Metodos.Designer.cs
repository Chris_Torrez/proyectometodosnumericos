﻿
namespace ProyectoMN
{
    partial class Metodos
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Metodos));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.TabControl = new System.Windows.Forms.TabControl();
            this.Biseccion = new System.Windows.Forms.TabPage();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.dgvbiseccion = new System.Windows.Forms.DataGridView();
            this.ReglaFalsa = new System.Windows.Forms.TabPage();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.dgvreglafalsa = new System.Windows.Forms.DataGridView();
            this.Secante = new System.Windows.Forms.TabPage();
            this.dataGridView4 = new System.Windows.Forms.DataGridView();
            this.NewtonRaphson = new System.Windows.Forms.TabPage();
            this.dgvrapshon = new System.Windows.Forms.DataGridView();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.button2 = new System.Windows.Forms.Button();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.txtec2 = new System.Windows.Forms.TextBox();
            this.txtec1 = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.txtincremento = new System.Windows.Forms.TextBox();
            this.txtfinal = new System.Windows.Forms.TextBox();
            this.txtinicio = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtgrafico = new System.Windows.Forms.Label();
            this.TablaGrafico = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.dataSet1 = new System.Data.DataSet();
            this.dataTable1 = new System.Data.DataTable();
            this.dataColumn1 = new System.Data.DataColumn();
            this.dataColumn2 = new System.Data.DataColumn();
            this.dataColumn3 = new System.Data.DataColumn();
            this.dataColumn4 = new System.Data.DataColumn();
            this.dataColumn5 = new System.Data.DataColumn();
            this.dataColumn6 = new System.Data.DataColumn();
            this.dataColumn7 = new System.Data.DataColumn();
            this.dataColumn8 = new System.Data.DataColumn();
            this.dataColumn9 = new System.Data.DataColumn();
            this.dataSet2 = new System.Data.DataSet();
            this.dataTable2 = new System.Data.DataTable();
            this.dataColumn10 = new System.Data.DataColumn();
            this.dataColumn11 = new System.Data.DataColumn();
            this.dataColumn12 = new System.Data.DataColumn();
            this.dataColumn13 = new System.Data.DataColumn();
            this.dataColumn14 = new System.Data.DataColumn();
            this.TabControl.SuspendLayout();
            this.Biseccion.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvbiseccion)).BeginInit();
            this.ReglaFalsa.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvreglafalsa)).BeginInit();
            this.Secante.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).BeginInit();
            this.NewtonRaphson.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvrapshon)).BeginInit();
            this.tabPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TablaGrafico)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).BeginInit();
            this.SuspendLayout();
            // 
            // TabControl
            // 
            this.TabControl.Controls.Add(this.Biseccion);
            this.TabControl.Controls.Add(this.ReglaFalsa);
            this.TabControl.Controls.Add(this.Secante);
            this.TabControl.Controls.Add(this.NewtonRaphson);
            this.TabControl.Controls.Add(this.tabPage1);
            this.TabControl.Location = new System.Drawing.Point(70, 35);
            this.TabControl.Name = "TabControl";
            this.TabControl.SelectedIndex = 0;
            this.TabControl.Size = new System.Drawing.Size(1487, 934);
            this.TabControl.TabIndex = 0;
            // 
            // Biseccion
            // 
            this.Biseccion.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Biseccion.BackgroundImage")));
            this.Biseccion.Controls.Add(this.dataGridView1);
            this.Biseccion.Controls.Add(this.dgvbiseccion);
            this.Biseccion.Location = new System.Drawing.Point(4, 25);
            this.Biseccion.Name = "Biseccion";
            this.Biseccion.Padding = new System.Windows.Forms.Padding(3);
            this.Biseccion.Size = new System.Drawing.Size(1479, 905);
            this.Biseccion.TabIndex = 0;
            this.Biseccion.Text = "Biseccion";
            this.Biseccion.UseVisualStyleBackColor = true;
            // 
            // dataGridView1
            // 
            this.dataGridView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dataGridView1.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView1.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Location = new System.Drawing.Point(49, 22);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(1367, 70);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellContentClick);
            // 
            // dgvbiseccion
            // 
            this.dgvbiseccion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvbiseccion.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvbiseccion.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvbiseccion.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvbiseccion.Location = new System.Drawing.Point(49, 126);
            this.dgvbiseccion.Name = "dgvbiseccion";
            this.dgvbiseccion.RowHeadersWidth = 51;
            this.dgvbiseccion.RowTemplate.Height = 24;
            this.dgvbiseccion.Size = new System.Drawing.Size(1367, 628);
            this.dgvbiseccion.TabIndex = 0;
            // 
            // ReglaFalsa
            // 
            this.ReglaFalsa.BackColor = System.Drawing.Color.Transparent;
            this.ReglaFalsa.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("ReglaFalsa.BackgroundImage")));
            this.ReglaFalsa.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ReglaFalsa.Controls.Add(this.dataGridView2);
            this.ReglaFalsa.Controls.Add(this.dgvreglafalsa);
            this.ReglaFalsa.Location = new System.Drawing.Point(4, 25);
            this.ReglaFalsa.Name = "ReglaFalsa";
            this.ReglaFalsa.Padding = new System.Windows.Forms.Padding(3);
            this.ReglaFalsa.Size = new System.Drawing.Size(1479, 905);
            this.ReglaFalsa.TabIndex = 1;
            this.ReglaFalsa.Text = "ReglaFalsa";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView2.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView2.Location = new System.Drawing.Point(44, 36);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.RowHeadersWidth = 51;
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(1367, 70);
            this.dataGridView2.TabIndex = 4;
            // 
            // dgvreglafalsa
            // 
            this.dgvreglafalsa.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvreglafalsa.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvreglafalsa.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvreglafalsa.Location = new System.Drawing.Point(44, 131);
            this.dgvreglafalsa.Name = "dgvreglafalsa";
            this.dgvreglafalsa.RowHeadersWidth = 51;
            this.dgvreglafalsa.RowTemplate.Height = 24;
            this.dgvreglafalsa.Size = new System.Drawing.Size(1367, 628);
            this.dgvreglafalsa.TabIndex = 3;
            // 
            // Secante
            // 
            this.Secante.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("Secante.BackgroundImage")));
            this.Secante.Controls.Add(this.dataGridView4);
            this.Secante.Location = new System.Drawing.Point(4, 25);
            this.Secante.Name = "Secante";
            this.Secante.Padding = new System.Windows.Forms.Padding(3);
            this.Secante.Size = new System.Drawing.Size(1479, 905);
            this.Secante.TabIndex = 2;
            this.Secante.Text = "Secante";
            this.Secante.UseVisualStyleBackColor = true;
            // 
            // dataGridView4
            // 
            this.dataGridView4.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridView4.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dataGridView4.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView4.Location = new System.Drawing.Point(39, 83);
            this.dataGridView4.Name = "dataGridView4";
            this.dataGridView4.RowHeadersWidth = 51;
            this.dataGridView4.RowTemplate.Height = 24;
            this.dataGridView4.Size = new System.Drawing.Size(1243, 751);
            this.dataGridView4.TabIndex = 0;
            // 
            // NewtonRaphson
            // 
            this.NewtonRaphson.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("NewtonRaphson.BackgroundImage")));
            this.NewtonRaphson.Controls.Add(this.dgvrapshon);
            this.NewtonRaphson.Location = new System.Drawing.Point(4, 25);
            this.NewtonRaphson.Name = "NewtonRaphson";
            this.NewtonRaphson.Padding = new System.Windows.Forms.Padding(3);
            this.NewtonRaphson.Size = new System.Drawing.Size(1479, 905);
            this.NewtonRaphson.TabIndex = 3;
            this.NewtonRaphson.Text = "Newton Raphson";
            this.NewtonRaphson.UseVisualStyleBackColor = true;
            // 
            // dgvrapshon
            // 
            this.dgvrapshon.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvrapshon.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvrapshon.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllCells;
            this.dgvrapshon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvrapshon.Location = new System.Drawing.Point(27, 58);
            this.dgvrapshon.Name = "dgvrapshon";
            this.dgvrapshon.RowHeadersWidth = 51;
            this.dgvrapshon.RowTemplate.Height = 24;
            this.dgvrapshon.Size = new System.Drawing.Size(1367, 628);
            this.dgvrapshon.TabIndex = 1;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.button2);
            this.tabPage1.Controls.Add(this.label7);
            this.tabPage1.Controls.Add(this.label6);
            this.tabPage1.Controls.Add(this.txtec2);
            this.tabPage1.Controls.Add(this.txtec1);
            this.tabPage1.Controls.Add(this.label5);
            this.tabPage1.Controls.Add(this.button1);
            this.tabPage1.Controls.Add(this.txtincremento);
            this.tabPage1.Controls.Add(this.txtfinal);
            this.tabPage1.Controls.Add(this.txtinicio);
            this.tabPage1.Controls.Add(this.label4);
            this.tabPage1.Controls.Add(this.label3);
            this.tabPage1.Controls.Add(this.label2);
            this.tabPage1.Controls.Add(this.label1);
            this.tabPage1.Controls.Add(this.txtgrafico);
            this.tabPage1.Controls.Add(this.TablaGrafico);
            this.tabPage1.Location = new System.Drawing.Point(4, 25);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1479, 905);
            this.tabPage1.TabIndex = 4;
            this.tabPage1.Text = "Gráfico";
            this.tabPage1.UseVisualStyleBackColor = true;
            this.tabPage1.Click += new System.EventHandler(this.tabPage1_Click);
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(1337, 81);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(110, 45);
            this.button2.TabIndex = 68;
            this.button2.Text = "Limpiar";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.BackColor = System.Drawing.Color.Transparent;
            this.label7.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(749, 344);
            this.label7.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(69, 31);
            this.label7.TabIndex = 67;
            this.label7.Text = "Ec. 2";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.BackColor = System.Drawing.Color.Transparent;
            this.label6.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(749, 304);
            this.label6.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(69, 31);
            this.label6.TabIndex = 66;
            this.label6.Text = "Ec. 1";
            // 
            // txtec2
            // 
            this.txtec2.Location = new System.Drawing.Point(857, 353);
            this.txtec2.Margin = new System.Windows.Forms.Padding(4);
            this.txtec2.Name = "txtec2";
            this.txtec2.Size = new System.Drawing.Size(200, 22);
            this.txtec2.TabIndex = 65;
            // 
            // txtec1
            // 
            this.txtec1.Location = new System.Drawing.Point(857, 313);
            this.txtec1.Margin = new System.Windows.Forms.Padding(4);
            this.txtec1.Name = "txtec1";
            this.txtec1.Size = new System.Drawing.Size(200, 22);
            this.txtec1.TabIndex = 64;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.BackColor = System.Drawing.Color.Transparent;
            this.label5.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(867, 262);
            this.label5.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(165, 36);
            this.label5.TabIndex = 63;
            this.label5.Text = "Ecuaciones";
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(1337, 19);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(110, 45);
            this.button1.TabIndex = 62;
            this.button1.Text = "Calcular";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // txtincremento
            // 
            this.txtincremento.Location = new System.Drawing.Point(1058, 94);
            this.txtincremento.Margin = new System.Windows.Forms.Padding(4);
            this.txtincremento.Name = "txtincremento";
            this.txtincremento.Size = new System.Drawing.Size(200, 22);
            this.txtincremento.TabIndex = 60;
            // 
            // txtfinal
            // 
            this.txtfinal.Location = new System.Drawing.Point(832, 94);
            this.txtfinal.Margin = new System.Windows.Forms.Padding(4);
            this.txtfinal.Name = "txtfinal";
            this.txtfinal.Size = new System.Drawing.Size(200, 22);
            this.txtfinal.TabIndex = 59;
            // 
            // txtinicio
            // 
            this.txtinicio.Location = new System.Drawing.Point(832, 52);
            this.txtinicio.Margin = new System.Windows.Forms.Padding(4);
            this.txtinicio.Name = "txtinicio";
            this.txtinicio.Size = new System.Drawing.Size(200, 22);
            this.txtinicio.TabIndex = 58;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.BackColor = System.Drawing.Color.Transparent;
            this.label4.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(1052, 43);
            this.label4.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(154, 31);
            this.label4.TabIndex = 57;
            this.label4.Text = "Incremento";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Transparent;
            this.label3.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(744, 85);
            this.label3.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(71, 31);
            this.label3.TabIndex = 56;
            this.label3.Text = "Final";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Transparent;
            this.label2.Font = new System.Drawing.Font("Microsoft YaHei", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(744, 43);
            this.label2.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 31);
            this.label2.TabIndex = 55;
            this.label2.Text = "Inicio";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Font = new System.Drawing.Font("Microsoft YaHei", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(846, 3);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(151, 36);
            this.label1.TabIndex = 54;
            this.label1.Text = "Intervalos";
            // 
            // txtgrafico
            // 
            this.txtgrafico.AutoSize = true;
            this.txtgrafico.BackColor = System.Drawing.Color.Transparent;
            this.txtgrafico.Font = new System.Drawing.Font("Microsoft YaHei", 28.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtgrafico.Location = new System.Drawing.Point(794, 154);
            this.txtgrafico.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.txtgrafico.Name = "txtgrafico";
            this.txtgrafico.Size = new System.Drawing.Size(203, 64);
            this.txtgrafico.TabIndex = 53;
            this.txtgrafico.Text = "Gráfico";
            // 
            // TablaGrafico
            // 
            chartArea1.Name = "ChartArea1";
            this.TablaGrafico.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.TablaGrafico.Legends.Add(legend1);
            this.TablaGrafico.Location = new System.Drawing.Point(37, 19);
            this.TablaGrafico.Name = "TablaGrafico";
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Legend = "Legend1";
            series1.Name = "Ecuacion 1";
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "Legend1";
            series2.Name = "Ecuacion 2";
            this.TablaGrafico.Series.Add(series1);
            this.TablaGrafico.Series.Add(series2);
            this.TablaGrafico.Size = new System.Drawing.Size(571, 819);
            this.TablaGrafico.TabIndex = 0;
            this.TablaGrafico.Text = "chart1";
            this.TablaGrafico.Click += new System.EventHandler(this.TablaGrafico_Click);
            // 
            // dataSet1
            // 
            this.dataSet1.DataSetName = "NewDataSet";
            this.dataSet1.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable1});
            // 
            // dataTable1
            // 
            this.dataTable1.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn1,
            this.dataColumn2,
            this.dataColumn3,
            this.dataColumn4,
            this.dataColumn5,
            this.dataColumn6,
            this.dataColumn7,
            this.dataColumn8,
            this.dataColumn9});
            this.dataTable1.TableName = "valoresbiseccion";
            // 
            // dataColumn1
            // 
            this.dataColumn1.ColumnName = "IT";
            // 
            // dataColumn2
            // 
            this.dataColumn2.ColumnName = "A";
            // 
            // dataColumn3
            // 
            this.dataColumn3.ColumnName = "B";
            // 
            // dataColumn4
            // 
            this.dataColumn4.ColumnName = "PM";
            // 
            // dataColumn5
            // 
            this.dataColumn5.ColumnName = "F(a)";
            // 
            // dataColumn6
            // 
            this.dataColumn6.ColumnName = "F(pm)";
            // 
            // dataColumn7
            // 
            this.dataColumn7.ColumnName = "F(a)*F(pm)";
            // 
            // dataColumn8
            // 
            this.dataColumn8.ColumnName = "Error";
            // 
            // dataColumn9
            // 
            this.dataColumn9.ColumnName = "Criterio De Parada";
            // 
            // dataSet2
            // 
            this.dataSet2.DataSetName = "NewDataSet";
            this.dataSet2.Tables.AddRange(new System.Data.DataTable[] {
            this.dataTable2});
            // 
            // dataTable2
            // 
            this.dataTable2.Columns.AddRange(new System.Data.DataColumn[] {
            this.dataColumn10,
            this.dataColumn11,
            this.dataColumn12,
            this.dataColumn13,
            this.dataColumn14});
            this.dataTable2.TableName = "Table1";
            // 
            // dataColumn10
            // 
            this.dataColumn10.Caption = "A";
            this.dataColumn10.ColumnName = "Column1";
            // 
            // dataColumn11
            // 
            this.dataColumn11.Caption = "B";
            this.dataColumn11.ColumnName = "Column2";
            // 
            // dataColumn12
            // 
            this.dataColumn12.Caption = "F(a)";
            this.dataColumn12.ColumnName = "Column3";
            // 
            // dataColumn13
            // 
            this.dataColumn13.Caption = "F(b)";
            this.dataColumn13.ColumnName = "Column4";
            // 
            // dataColumn14
            // 
            this.dataColumn14.Caption = "F(a*b)";
            this.dataColumn14.ColumnName = "Column5";
            // 
            // Metodos
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImage = ((System.Drawing.Image)(resources.GetObject("$this.BackgroundImage")));
            this.ClientSize = new System.Drawing.Size(1585, 1055);
            this.Controls.Add(this.TabControl);
            this.Name = "Metodos";
            this.Text = "Form3";
            this.Load += new System.EventHandler(this.Metodos_Load);
            this.TabControl.ResumeLayout(false);
            this.Biseccion.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvbiseccion)).EndInit();
            this.ReglaFalsa.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvreglafalsa)).EndInit();
            this.Secante.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView4)).EndInit();
            this.NewtonRaphson.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvrapshon)).EndInit();
            this.tabPage1.ResumeLayout(false);
            this.tabPage1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TablaGrafico)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataSet2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataTable2)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl TabControl;
        private System.Windows.Forms.TabPage Biseccion;
        private System.Windows.Forms.TabPage ReglaFalsa;
        private System.Windows.Forms.TabPage Secante;
        private System.Windows.Forms.TabPage NewtonRaphson;
        private System.Windows.Forms.DataGridView dgvbiseccion;
        private System.Data.DataSet dataSet1;
        private System.Data.DataTable dataTable1;
        private System.Data.DataColumn dataColumn1;
        private System.Data.DataColumn dataColumn2;
        private System.Data.DataColumn dataColumn3;
        private System.Data.DataColumn dataColumn4;
        private System.Data.DataColumn dataColumn5;
        private System.Data.DataColumn dataColumn6;
        private System.Data.DataColumn dataColumn7;
        private System.Data.DataColumn dataColumn8;
        private System.Data.DataColumn dataColumn9;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Data.DataSet dataSet2;
        private System.Data.DataTable dataTable2;
        private System.Data.DataColumn dataColumn10;
        private System.Data.DataColumn dataColumn11;
        private System.Data.DataColumn dataColumn12;
        private System.Data.DataColumn dataColumn13;
        private System.Data.DataColumn dataColumn14;
        private System.Windows.Forms.DataGridView dgvreglafalsa;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.DataGridView dataGridView4;
        private System.Windows.Forms.DataGridView dgvrapshon;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.DataVisualization.Charting.Chart TablaGrafico;
        private System.Windows.Forms.Label txtgrafico;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtincremento;
        private System.Windows.Forms.TextBox txtfinal;
        private System.Windows.Forms.TextBox txtinicio;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtec2;
        private System.Windows.Forms.TextBox txtec1;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Button button2;
    }
}