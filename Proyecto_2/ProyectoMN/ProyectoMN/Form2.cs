﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using info.lundin.math;
using System.Collections;

namespace ProyectoMN
{
    public partial class txtxo : Form
    {


        public txtxo()
        {
            InitializeComponent();
        }



        public void reglafalsa()
        {
            // declaramos la variable del form metodos

            Metodos variable = new Metodos();

            // declarar variables

            double error;
            double x;
            double x1;
            double xr;
            double fxa;
            double fxb;
            double fxab;
            string ecuacion;
            double criteriodeparada;
#pragma warning disable CS0168 // La variable 'pm2' se ha declarado pero nunca se usa
            double pm2;
#pragma warning restore CS0168 // La variable 'pm2' se ha declarado pero nunca se usa
            double it;
#pragma warning disable CS0168 // La variable 'fxf' se ha declarado pero nunca se usa
            double fxf;
#pragma warning restore CS0168 // La variable 'fxf' se ha declarado pero nunca se usa
            double fxr;
            double xpos;


            it = 0;

            String a;
            String b;
            String c;
#pragma warning disable CS0168 // La variable 'pm1' se ha declarado pero nunca se usa
            String pm1;
#pragma warning restore CS0168 // La variable 'pm1' se ha declarado pero nunca se usa
            String fxa1;
            String fxb1;
            String fxc1;
            String fxab1;
#pragma warning disable CS0168 // La variable 'Criterio' se ha declarado pero nunca se usa
            String Criterio;
#pragma warning restore CS0168 // La variable 'Criterio' se ha declarado pero nunca se usa
            String Error;
            String condicion;
            String It1;

            error = Convert.ToDouble(txterror.Text);
            x = Convert.ToDouble(txtintervalo1.Text);
            x1 = Convert.ToDouble(txtintervalo2.Text);

            criteriodeparada = 0;

            ecuacion = TxtPantallaCalculo.Text;

            DoubleValue xval = new DoubleValue();
            ExpressionParser parser = new ExpressionParser();
            parser.Values.Add("x", xval);

            //Intervalo1 

            xval.Value = x; // Update value of "x"
            double result = parser.Parse(ecuacion);
            fxa = result;


            //Intervalo2
            xval.Value = x1; // Update value of "x"
            result = parser.Parse(ecuacion);
            fxb = result;



            // Calculo de Xr 

            xr = x - ((fxa * (x1 - x)) / (fxb - fxa));


            //Intervalo3
            xval.Value = xr; // Update value of "x"
            result = parser.Parse(ecuacion);
            fxr = result;



            //Llenar variables



            a = txtintervalo1.Text;
            b = txtintervalo2.Text;
            c = Convert.ToString(xr);
            fxab = fxa * fxr;
            fxa1 = Convert.ToString(fxa);
            fxb1 = Convert.ToString(fxb);
            fxab1 = Convert.ToString(fxab);
            fxc1 = Convert.ToString(fxr);
            Error = Convert.ToString(criteriodeparada);
            condicion = Convert.ToString(error);
            ++it;
            It1 = Convert.ToString(it);

            //Llenar el datatable

            DataTable dtreglafalsa = dataSet5.Tables[0];
            DataRow drreglafalsa = dataSet5.Tables[0].NewRow();
            drreglafalsa[0] = It1;
            drreglafalsa[1] = a;
            drreglafalsa[2] = b;
            drreglafalsa[3] = c;
            drreglafalsa[4] = fxa1;
            drreglafalsa[5] = fxb1;
            drreglafalsa[6] = fxc1;
            drreglafalsa[7] = fxab1;
            drreglafalsa[8] = Error;
            drreglafalsa[9] = condicion;

            dtreglafalsa.Rows.Add(drreglafalsa);

            // condicion
            if (fxab>0)
            {
                x = xr;
            }
            else
            {
#pragma warning disable CS1717 // Asignación a la misma variable. ¿Quería asignar otro elemento?
                x = x;
#pragma warning restore CS1717 // Asignación a la misma variable. ¿Quería asignar otro elemento?
            }

            if (fxab > 0)
            {
#pragma warning disable CS1717 // Asignación a la misma variable. ¿Quería asignar otro elemento?
                x1 = x1;
#pragma warning restore CS1717 // Asignación a la misma variable. ¿Quería asignar otro elemento?
            }
            else
            {
                x1 = xr ;
            }
            xpos = xr;

            do {         
            //Intervalo1 

            xval.Value = x; // Update value of "x"
            result = parser.Parse(ecuacion);
            fxa = result;


            //Intervalo2
            xval.Value = x1; // Update value of "x"
            result = parser.Parse(ecuacion);
            fxb = result;



            // Calculo de Xr 

            xr = x - ((fxa * (x1 - x)) / (fxb - fxa));


            //Intervalo3
            xval.Value = xr; // Update value of "x"
            result = parser.Parse(ecuacion);
            fxr = result;




            error = Math.Abs(xpos - xr);

            criteriodeparada = Convert.ToDouble(txterror.Text);

            if (Math.Abs(error) > Math.Abs(criteriodeparada))
            {
                condicion = "Falso";

            }
            else
            {
                condicion = "Verdadero";
            }



            //Llenar variables



            a = Convert.ToString(x);
            b = Convert.ToString(x1);
            c = Convert.ToString(xr);
            fxab = fxa * fxr;
            fxa1 = Convert.ToString(fxa);
            fxb1 = Convert.ToString(fxb);
            fxab1 = Convert.ToString(fxab);
            fxc1 = Convert.ToString(fxr);
            Error = Convert.ToString(error);
            ++it;
            It1 = Convert.ToString(it);

            //Llenar el datatable

            dtreglafalsa = dataSet5.Tables[0];
            drreglafalsa = dataSet5.Tables[0].NewRow();
            drreglafalsa[0] = It1;
            drreglafalsa[1] = a;
            drreglafalsa[2] = b;
            drreglafalsa[3] = c;
            drreglafalsa[4] = fxa1;
            drreglafalsa[5] = fxb1;
            drreglafalsa[6] = fxc1;
            drreglafalsa[7] = fxab1;
            drreglafalsa[8] = Error;
            drreglafalsa[9] = condicion;

            dtreglafalsa.Rows.Add(drreglafalsa);


            // condicion
            if (fxab > 0)
            {
                x = xr;
            }
            else
            {
#pragma warning disable CS1717 // Asignación a la misma variable. ¿Quería asignar otro elemento?
                x = x;
#pragma warning restore CS1717 // Asignación a la misma variable. ¿Quería asignar otro elemento?
            }

            if (fxab > 0)
            {
#pragma warning disable CS1717 // Asignación a la misma variable. ¿Quería asignar otro elemento?
                x1 = x1;
#pragma warning restore CS1717 // Asignación a la misma variable. ¿Quería asignar otro elemento?
            }
            else
            {
                x1 = xr;
            }

            xpos = xr;

        } while (Math.Abs(error) > Math.Abs(criteriodeparada)) ;


        }

    public void newton()
        {
            double x;
            double fxa;
            string ecuacion2;
            double error;
            double criteriodeparada;
            double it = 0;
            double delta = 0.001;
            double derivada;
            double xmax;
            double xmin;
            double fxmax;
            double fxmin;
            double xpos;

            String a;
#pragma warning disable CS0168 // La variable 'b' se ha declarado pero nunca se usa
            String b;
#pragma warning restore CS0168 // La variable 'b' se ha declarado pero nunca se usa
#pragma warning disable CS0168 // La variable 'pm1' se ha declarado pero nunca se usa
            String pm1;
#pragma warning restore CS0168 // La variable 'pm1' se ha declarado pero nunca se usa
            String fxa1;
#pragma warning disable CS0168 // La variable 'fxb1' se ha declarado pero nunca se usa
            String fxb1;
#pragma warning restore CS0168 // La variable 'fxb1' se ha declarado pero nunca se usa
#pragma warning disable CS0168 // La variable 'fxab1' se ha declarado pero nunca se usa
            String fxab1;
#pragma warning restore CS0168 // La variable 'fxab1' se ha declarado pero nunca se usa
#pragma warning disable CS0168 // La variable 'Criterio' se ha declarado pero nunca se usa
            String Criterio;
#pragma warning restore CS0168 // La variable 'Criterio' se ha declarado pero nunca se usa
            String Error;
#pragma warning disable CS0168 // La variable 'fpm1' se ha declarado pero nunca se usa
            String fpm1;
#pragma warning restore CS0168 // La variable 'fpm1' se ha declarado pero nunca se usa
            String condicion;
            String It1;
            String derivada1;


            error = Convert.ToDouble(txterror.Text);
            x = Convert.ToDouble(txtx0.Text);

            criteriodeparada = 0;

            ecuacion2 = TxtPantallaCalculo.Text;
           


            DoubleValue xval1 = new DoubleValue();
            ExpressionParser parser2 = new ExpressionParser();
            parser2.Values.Add("x", xval1);

            //Intervalo1 

            xval1.Value = x; // Update value of "x"
            double result = parser2.Parse(ecuacion2);
            fxa = result;

            // Evaluacion de la derivada

            xmax = x + delta;
            xmin = x - delta;

            xval1.Value = xmax; // Update value of "x"
            result = parser2.Parse(ecuacion2);
            fxmax = result;

            xval1.Value = xmin; // Update value of "x"
            result = parser2.Parse(ecuacion2);
            fxmin = result;

            derivada = ((fxmax - fxmin) / (2 * delta));


            //Llenar variables

            a = Convert.ToString(x);

            fxa1 = Convert.ToString(fxa);


            Error = Convert.ToString(criteriodeparada);

            condicion = Convert.ToString(error);
            ++it;
            It1 = Convert.ToString(it);
            derivada1 = Convert.ToString(derivada);


            //Llenar el datatable

            DataTable dtnewton = dataSet4.Tables[0];
            DataRow drnewton = dataSet4.Tables[0].NewRow();
            drnewton[0] = It1;
            drnewton[1] = a;
            drnewton[2] = fxa1;
            drnewton[3] = derivada1;
            drnewton[4] = Error;
            drnewton[5] = condicion;

            dtnewton.Rows.Add(drnewton);
            xpos = x;
            x = x - (fxa / derivada);



            // Condicion
            do { 

            //Intervalo1 

            xval1.Value = x; // Update value of "x"
            result = parser2.Parse(ecuacion2);
            fxa = result;

            // Evaluacion de la derivada

            xmax = x + delta;
            xmin = x - delta;

            xval1.Value = xmax; // Update value of "x"
            result = parser2.Parse(ecuacion2);
            fxmax = result;

            xval1.Value = xmin; // Update value of "x"
            result = parser2.Parse(ecuacion2);
            fxmin = result;

            derivada = ((fxmax - fxmin) / (2 * delta));


            // error



            error = Math.Abs(xpos - x);

            criteriodeparada = Convert.ToDouble(txterror.Text);

            if (Math.Abs(error) > Math.Abs(criteriodeparada))
            {
                condicion = "Falso";

            }
            else
            {
                condicion = "Verdadero";
            }



            //Llenar variables

            a = Convert.ToString(x);


            fxa1 = Convert.ToString(fxa);


            Error = condicion;

            condicion = Convert.ToString(error);
            ++it;
            It1 = Convert.ToString(it);
            derivada1 = Convert.ToString(derivada);



            //Llenar el datatable

            dtnewton = dataSet4.Tables[0];
            drnewton = dataSet4.Tables[0].NewRow();
            drnewton[0] = It1;
            drnewton[1] = a;
            drnewton[2] = fxa1;
            drnewton[3] = derivada1;
            drnewton[4] = condicion;
            drnewton[5] = Error;

            dtnewton.Rows.Add(drnewton);
            xpos = x;
            x = (x - (fxa / derivada));


        } while (Math.Abs(error) > Math.Abs(criteriodeparada)) ;


        }

        public void biseccion()
        {

            // declaramos la variable del form metodos

            Metodos variable = new Metodos();

            // declarar variables

            double error;
            double x;
            double x1;
            double fxa;
            double fxb;
            double fxab;
            string ecuacion;
            double criteriodeparada;
            double pm;
            double fpm;
#pragma warning disable CS0168 // La variable 'pm2' se ha declarado pero nunca se usa
            double pm2;
#pragma warning restore CS0168 // La variable 'pm2' se ha declarado pero nunca se usa
            double it;
#pragma warning disable CS0168 // La variable 'fxf' se ha declarado pero nunca se usa
            double fxf;
#pragma warning restore CS0168 // La variable 'fxf' se ha declarado pero nunca se usa

            double it2;
            double xpos;
            it2 = 0;

            it = 0;

            String a;
            String b;
            String pm1;
            String fxa1;
            String fxb1;
            String fxab1;
#pragma warning disable CS0168 // La variable 'Criterio' se ha declarado pero nunca se usa
            String Criterio;
#pragma warning restore CS0168 // La variable 'Criterio' se ha declarado pero nunca se usa
            String Error;
            String fpm1;
            String condicion;
            String It1;

            error = Convert.ToDouble(txterror.Text);
            x = Convert.ToDouble(txtintervalo1.Text);
            x1 = Convert.ToDouble(txtintervalo2.Text);
            pm = (x + x1) / 2;
            criteriodeparada = 0;

            ecuacion = TxtPantallaCalculo.Text;

            DoubleValue xval = new DoubleValue();
            ExpressionParser parser = new ExpressionParser();
            parser.Values.Add("x", xval);




            //Intervalo1 

            xval.Value = x; // Update value of "x"
            double result = parser.Parse(ecuacion);
            fxa = result;

           


            //Intervalo2
            xval.Value = x1; // Update value of "x"
            result = parser.Parse(ecuacion);
            fxb = result;

            //PuntoMedio
            xval.Value = pm; // Update value of "x"
            result = parser.Parse(ecuacion);
            fpm = result;

            //Llenar variables

           

            a = txtintervalo1.Text;
            b = txtintervalo2.Text;
            pm1 = Convert.ToString(pm);
            fxab = fxa * fpm;
            fxa1 = Convert.ToString(fxa);
            fxb1 = Convert.ToString(fxb);
            fxab1 = Convert.ToString(fxab);
            Error = Convert.ToString(criteriodeparada);
            fpm1 = Convert.ToString(fpm);
            condicion = Convert.ToString(error);
            ++it;
            It1 = Convert.ToString(it);
            
            //Llenar el datatable

            DataTable dtbiseccion = dataSet1.Tables[0];
            DataRow drbiseccion = dataSet1.Tables[0].NewRow();
            drbiseccion[0] = It1;
            drbiseccion[1] = a;
            drbiseccion[2] = b;
            drbiseccion[3] = pm;
            drbiseccion[4] = fxa1;
            drbiseccion[5] = fpm1;
            drbiseccion[6] = fxab1;
            drbiseccion[7] = Error;
            drbiseccion[8] = condicion;

            dtbiseccion.Rows.Add(drbiseccion);
            


            // Coneccion del form
            Metodos form = new Metodos();




            //Condicion 

            if (fxab > 0)
            {
                x = pm;
            }
            else
            {
#pragma warning disable CS1717 // Asignación a la misma variable. ¿Quería asignar otro elemento?
                x = x;
#pragma warning restore CS1717 // Asignación a la misma variable. ¿Quería asignar otro elemento?
            }

            if (fxab > 0)
            {
#pragma warning disable CS1717 // Asignación a la misma variable. ¿Quería asignar otro elemento?
                x1 = x1;
#pragma warning restore CS1717 // Asignación a la misma variable. ¿Quería asignar otro elemento?
            }
            else
            {
                x1 = pm;
            }
            xpos = pm;


            do { 

            //Intervalo1 

            xval.Value = x; // Update value of "x"
            result = parser.Parse(ecuacion);
            fxa = result;


            //Intervalo2
            xval.Value = x1; // Update value of "x"
            result = parser.Parse(ecuacion);
            fxb = result;

           

            pm = (x + x1) / 2;



            //PuntoMedio
            xval.Value = pm; // Update value of "x"
            result = parser.Parse(ecuacion);
            fpm = result;
            error = Math.Abs(xpos - pm);
                criteriodeparada = Convert.ToDouble(txterror.Text);

            if (Math.Abs(error) > Math.Abs(criteriodeparada))
            {
                condicion = "Falso";

            }
            else
            {
                condicion = "Verdadero";
            }


            ++it;
            It1 = Convert.ToString(it);
            criteriodeparada = xpos - pm;
            Error = Convert.ToString(error);


            a = Convert.ToString(x);
            b = Convert.ToString(x1);
            pm1 = Convert.ToString(pm);
            fxab = fxa * fpm;
            fxa1 = Convert.ToString(fxa);
            fxb1 = Convert.ToString(fxb);
            fxab1 = Convert.ToString(fxab);
            fpm1 = Convert.ToString(fpm);

           

            

            ++it2;

            dtbiseccion = dataSet1.Tables[0];
            drbiseccion = dataSet1.Tables[0].NewRow();

            drbiseccion[0] = It1;
            drbiseccion[1] = a;
            drbiseccion[2] = b;
            drbiseccion[3] = pm;
            drbiseccion[4] = fxa1;
            drbiseccion[5] = fpm1;
            drbiseccion[6] = fxab1;
            drbiseccion[7] = Error;
            drbiseccion[8] = condicion;
            dtbiseccion.Rows.Add(drbiseccion);

            if (fxab > 0)
            {
                x = pm;
            }
            else
            {
#pragma warning disable CS1717 // Asignación a la misma variable. ¿Quería asignar otro elemento?
                x = x;
#pragma warning restore CS1717 // Asignación a la misma variable. ¿Quería asignar otro elemento?
            }

            if (fxab > 0)
            {
#pragma warning disable CS1717 // Asignación a la misma variable. ¿Quería asignar otro elemento?
                x1 = x1;
#pragma warning restore CS1717 // Asignación a la misma variable. ¿Quería asignar otro elemento?
            }
            else
            {
                x1 = pm;
            }
            xpos = pm;

        } while( condicion.Equals("Falso"));




        }






        public void calculo()
        {
            try { 

            double x;
            double x1;
            double fxa;
            double fxb;
            double fxab;
            string ecuacion;

            String a;
            String b;
            String fxa1;
            String fxb1;
            String fxab1;


            x = Convert.ToDouble(txtintervalo1.Text);
            x1 = Convert.ToDouble(txtintervalo2.Text);
            ecuacion = TxtPantallaCalculo.Text;

            DoubleValue xval = new DoubleValue();
            ExpressionParser parser = new ExpressionParser();
            parser.Values.Add("x", xval);

            //Intervalo1 

            xval.Value = x; // Update value of "x"
            double result = parser.Parse(ecuacion);
            fxa = result;




            //Intervalo2
            xval.Value = x1; // Update value of "x"
            result = parser.Parse(ecuacion);
            fxb = result;



            // variables datagriwview
            fxab = fxa * fxb;
           

            if (fxa<0 && fxb<0){

                fxab1 = "No se puede realizar";

            } else if (fxa > 0 && fxb > 0)
            {
                fxab1 = "No se puede realizar";
            }
            else
            {
                fxab1 = Convert.ToString(fxab);
            }

            fxa1 = Convert.ToString(fxa);
            fxb1 = Convert.ToString(fxb);
           
            a = txtintervalo1.Text;
            b = txtintervalo2.Text;






            DataTable dt = dataSet2.Tables[0];
            DataRow dr = dataSet2.Tables[0].NewRow();
            dr[0] = a;
            dr[1] = b;
            dr[2] = fxa1;
            dr[3] = fxb1;
            dr[4] = fxab1;

            dt.Rows.Add(dr);





            // Coneccion del form
            Metodos form = new Metodos();
            }

            catch (System.IO.IOException ex)
            {
                MessageBox.Show(ex.Message);
            }





        }

        void Secante()
        {

            // declaramos la variable del form metodos

            Metodos variable = new Metodos();

            // declarar variables

            double error;
            double x;
            double x1;
            double fxa;
            double fxb;
            string ecuacion;
            double criteriodeparada;
            double it;
            double xpos;
            double fxpos;
       
            it = 0;

            String a;
            String b;
            
            String fxa1;
            String fxb1;
#pragma warning disable CS0168 // La variable 'fxab1' se ha declarado pero nunca se usa
            String fxab1;
#pragma warning restore CS0168 // La variable 'fxab1' se ha declarado pero nunca se usa
#pragma warning disable CS0168 // La variable 'Criterio' se ha declarado pero nunca se usa
            String Criterio;
#pragma warning restore CS0168 // La variable 'Criterio' se ha declarado pero nunca se usa
            String Error;    
            String It1;
            String condicion;

            error = Convert.ToDouble(txterror.Text);
            
            criteriodeparada = 0;





            x = Convert.ToDouble(txtintervalo1.Text);
           

            x1 = Convert.ToDouble(txtintervalo2.Text);
            ecuacion = TxtPantallaCalculo.Text;
         

            DoubleValue xval = new DoubleValue();
            ExpressionParser parser = new ExpressionParser();
            parser.Values.Add("x", xval);

            //Intervalo1 

            xval.Value = x; // Update value of "x"
            double result = parser.Parse(ecuacion);
            fxa = result;


   

            //Llenar variables

            a = txtintervalo1.Text;
            b = txtintervalo2.Text;
            fxa1 = Convert.ToString(fxa);
           
            Error = Convert.ToString(criteriodeparada);
            condicion = Convert.ToString(error);
            ++it;
            It1 = Convert.ToString(it);

            //Llenar el datatable

            DataTable dtsecante= dataSet3.Tables[0];
            DataRow drsecante = dataSet3.Tables[0].NewRow();
            drsecante[0] = It1;
            drsecante[1] = a;
            drsecante[2] = fxa1;
            drsecante[3] = Error;
            drsecante[4] = condicion;
            dtsecante.Rows.Add(drsecante);


            //Intervalo2
            xval.Value = x1; // Update value of "x"
            result = parser.Parse(ecuacion);
            fxb = result;


            a = txtintervalo1.Text;
            b = txtintervalo2.Text;
    
            fxb1 = Convert.ToString(fxb);
           
            Error = Convert.ToString(criteriodeparada);
            condicion = Convert.ToString(error);
            ++it;
            It1 = Convert.ToString(it);

            //Llenar el datatable

             drsecante = dataSet3.Tables[0].NewRow();
            drsecante[0] = It1;
            drsecante[1] = b;
            drsecante[2] = fxb1;
            drsecante[3] = Error;
            drsecante[4] = condicion;
            dtsecante.Rows.Add(drsecante);

            fxpos = fxa;
            xpos = x;

            do
            {
                x = x1 - ((fxb * (x1 - xpos)) / (fxb - fxpos));

                xval.Value = x; // Update value of "x"
                result = parser.Parse(ecuacion);
                fxa = result;

                error = Math.Abs(x1 - x);

                criteriodeparada = Convert.ToDouble(txterror.Text);

               


                if (Math.Abs(error) > Math.Abs(criteriodeparada))
                {
                    condicion = "Falso";

                }
                else
                {
                    condicion = "Verdadero";
                }



                a = Convert.ToString(x);
                fxa1 = Convert.ToString(fxa);

                Error = Convert.ToString(error);

                ++it;
                It1 = Convert.ToString(it);
                drsecante = dataSet3.Tables[0].NewRow();
                drsecante[0] = It1;
                drsecante[1] = a;
                drsecante[2] = fxa1;
                drsecante[3] = Error;
                drsecante[4] = condicion;
                dtsecante.Rows.Add(drsecante);

                xpos = x1;
                fxpos = fxb;
                x1 = x;
                fxb = fxa;


            } while (Math.Abs(error) > Math.Abs(criteriodeparada));








        }










       


        private void TxtPantallaCalculo_TextChanged(object sender, EventArgs e)
        {
            



        }

        private void btnx_Click(object sender, EventArgs e)
        {

            
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "X";

        }

        private void BtnLimpiar_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = "";
        }

        private void btnn1_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "1";
        }

        private void btnn2_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "2";
        }

        private void btnn3_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "3";
        }

        private void btnn4_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "4";
        }

        private void btnn5_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "5";
        }

        private void btnn6_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "6";
        }

        private void btnn7_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "7";
        }

        private void btnn8_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "8";
        }

        private void btnn9_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "9";
        }

        private void btnn0_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "0";
        }

        private void btnsuma_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "+";
        }

        private void btndivision_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "/";
        }

        private void btnmenos_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "-";
        }

        private void btnmultiplicar_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "*";
        }


     

        private void btncalcular_Click_1(object sender, EventArgs e)
        {



            


        }

    

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void txtx0_TextChanged(object sender, EventArgs e)
        {

        }

        private void txterror_TextChanged(object sender, EventArgs e)
        {

        }

        private void Btnscoseno_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "COS(";
        }

        private void btncotangente_Click_1(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "ATAN(";
        }

        private void btnln_Click_1(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "LN(";
        }

        private void btntangente_Click_1(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "TAN(";
        }

        private void button1_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "sqrt(";
        }

        private void btncalcular_Click(object sender, EventArgs e)
        {
            try
            {



                string ecuacion;
                ecuacion = TxtPantallaCalculo.Text;




                dataSet2.Clear();
                calculo();
                TxtPantallaCalculo.Text = ecuacion;
                Metodos form = new Metodos();
                form.data2 = dataSet2;

                dataSet1.Clear();
                biseccion();
                TxtPantallaCalculo.Text = ecuacion;
                form.data = dataSet1;


                dataSet3.Clear();
                Secante();
                TxtPantallaCalculo.Text = ecuacion;
                form.data3 = dataSet3;

                dataSet4.Clear();
                newton();
                TxtPantallaCalculo.Text = ecuacion;
                form.data4 = dataSet4;


                dataSet5.Clear();
                reglafalsa();
                TxtPantallaCalculo.Text = ecuacion;
                form.data5 = dataSet5;


                form.Show();
            }
            catch (System.IO.IOException ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


       
     


        
        private void btnparentensis1_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "(";
        }

        private void btnparentesis2_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + ")";
        }

        private void btnelevar_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "^";
        }

        private void btnlog_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "Log(";
        }

        private void btnln_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "Ln(";
        }

        private void Btnseno_Click_1(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "SIN(";
        }

        private void Btnsecante_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "ACOS(";
        }

        private void btnlog_Click_1(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "LOG(";
        }

        private void Btnscosecante_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "ASIN(";
        }

        private void btne_Click(object sender, EventArgs e)
        {
            TxtPantallaCalculo.Text = TxtPantallaCalculo.Text + "EXP(";
        }

       

    }
}
